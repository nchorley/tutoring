package com.xyphias.tutoring

import org.http4k.template.ViewModel

class Students(val students: List<Student>): ViewModel

data class Totals(val overall: Int, val currentTaxYear: Int)
class Sessions(val sessions: List<Session>, val totals: Totals, val taxYear: Int): ViewModel

class Login(val error: String?): ViewModel
