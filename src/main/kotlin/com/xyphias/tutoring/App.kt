package com.xyphias.tutoring

import com.auth0.jwt.algorithms.Algorithm
import org.http4k.core.Method
import org.http4k.core.then
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.http4k.template.HandlebarsTemplates

fun main(args: Array<String>) {
    val dao = Dao(System.getenv("JDBC_DATABASE_URL"))
    val dev = System.getenv("DEV")?.toBoolean() ?: false
    val templateRenderer = HandlebarsTemplates().run {
        if (dev) Caching("src/main/resources/templates") else CachingClasspath("templates")
    }

    val tutoringService = TutoringService(dao, templateRenderer)
    val algorithm: Algorithm = Algorithm.HMAC256(System.getenv("SECRET_KEY"))
    val loginService = LoginService(System.getenv("PASSWORD"), algorithm, templateRenderer)
    val loginFilter = LoginFilter(algorithm)


    val protected = loginFilter.then(routes(
            "/students" bind routes(
                    Method.GET to tutoringService::getStudents,
                    Method.POST to tutoringService::addStudent
            ),
            "/students/{id:.*}" bind Method.DELETE to tutoringService::deleteStudent,
            "/sessions" bind routes(
                    Method.GET to tutoringService::getSessions,
                    Method.POST to tutoringService::addSession
            ),
            "/set-tax-year" bind Method.POST to tutoringService::setTaxYear,
            "/set-paid" bind Method.POST to tutoringService::setPaid,
            "/delete-sessions" bind Method.POST to tutoringService::deleteSessions
    ))

    val app = routes(
            "/login" bind routes(
                    Method.GET to loginService::getLoginPage,
                    Method.POST to loginService::login
            ),
            protected,
             "/db-update/{scriptName}" bind Method.POST to updateSchema(dao)
    )

    val port = (System.getenv("PORT") ?: "9000").toInt()

    app.asServer(Jetty(port)).start()
}

