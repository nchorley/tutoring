package com.xyphias.tutoring

import java.time.LocalDate

data class Student(val id: Int? = null, val name: String)

data class Session(val id: Int? = null,
                   val date: LocalDate,
                   val student: Student,
                   val amount: Int,
                   val paid: Boolean,
                   val taxYear: Int? = null
)

data class TaxYearUpdate(val year: Int, val sessionIds: Array<Int>)

data class PaidUpdate(val sessionId: Int, val paid: Boolean)

data class DeleteSessions(val sessionIds: Array<Int>)
