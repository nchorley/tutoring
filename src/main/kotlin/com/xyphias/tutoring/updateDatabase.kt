package com.xyphias.tutoring

import com.natpryce.Err
import com.natpryce.Ok
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.lens.Path

fun updateSchema(dao: Dao): (Request) -> Response = { request: Request ->
    val scriptName = Path.of("scriptName")

    dao.updateSchema(scriptName(request)).let {
        when (it) {
            is Ok<Unit> -> Response(Status.OK)
            is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body(it.reason.message.orEmpty())
        }
    }
}
