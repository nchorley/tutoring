package com.xyphias.tutoring

import com.natpryce.Err
import com.natpryce.Ok
import org.http4k.core.Body
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.format.Jackson.auto
import org.http4k.lens.*
import org.http4k.routing.path
import org.http4k.template.TemplateRenderer
import java.time.LocalDate

class TutoringService(private val dao: Dao, private val templateRenderer: TemplateRenderer) {
    fun getStudents(request: Request): Response =
        dao.getStudents().let {
            when (it) {
                is Ok<List<Student>> -> Response(Status.OK).body(templateRenderer(Students(it.value)))
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun addStudent(request: Request): Response =
        dao.addStudent(studentLens.extract(request)).let {
            when (it) {
                is Ok<Unit> -> Response(Status.FOUND).header("Location", "/students")
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun deleteStudent(request: Request): Response =
        dao.deleteStudent(request.path("id")!!.toInt()).let {
            when (it) {
                is Ok<Unit> -> Response(Status.OK)
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun getSessions(request: Request): Response =
        dao.getSessions().let {
            when (it) {
                is Ok<List<Session>> -> {
                    val currentTaxYear = taxYear(LocalDate.now())

                    val totals = it.value.fold(Totals(0, 0)) { acc, session ->
                        val currentTaxYearAmount = if (session.taxYear == currentTaxYear) {
                            acc.currentTaxYear + session.amount
                        } else {
                            acc.currentTaxYear
                        }

                        Totals(acc.overall + session.amount,  currentTaxYearAmount)
                    }

                    Response(Status.OK).body(templateRenderer(Sessions(it.value, totals, currentTaxYear)))
                }
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun deleteSessions(request: Request): Response =
        dao.deleteSessions(deleteSessionsLens.extract(request).sessionIds).let {
            when (it) {
                is Ok<Unit> -> Response(Status.OK)
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    private fun taxYear(date: LocalDate): Int = date.let {
        val april5th = LocalDate.of(it.year, 4, 5)

        if (it.isEqual(april5th) || it.isAfter(april5th)) {
            it.year + 1
        } else {
            it.year
        }
    }

    fun addSession(request: Request): Response =
        dao.addSession(sessionLens.extract(request)).let {
            when(it) {
                is Ok<Unit> -> Response(Status.FOUND).header("Location", "/sessions")
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun setTaxYear(request: Request): Response =
        dao.setTaxYear(taxUpdateLens.extract(request)).let {
            when (it) {
                is Ok<Unit> -> Response(Status.OK)
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    fun setPaid(request: Request): Response =
        dao.setPaid(paidUpdateLens.extract(request)).let {
            when (it) {
                is Ok<Unit> -> Response(Status.OK)
                is Err<Exception> -> Response(Status.INTERNAL_SERVER_ERROR).body("${it.reason.message}")
            }
        }

    private val studentName = FormField.string().required("studentName")
    private val studentLens = Body.webForm(Validator.Strict, studentName).map { Student(name = studentName(it)) }.toLens()
    private val taxUpdateLens = Body.auto<TaxYearUpdate>().toLens()
    private val paidUpdateLens = Body.auto<PaidUpdate>().toLens()
    private val deleteSessionsLens = Body.auto<DeleteSessions>().toLens()

    private val sessionStudentId = FormField.int().required("studentId")
    private val sessionStudentName = FormField.string().required("studentName")
    private val sessionAmount = FormField.int().required("amount")
    private val sessionDate = FormField.localDate().required("date")

    private val sessionLens = Body.webForm(Validator.Strict, sessionStudentId, sessionStudentName, sessionAmount, sessionDate).map {
        Session(date = sessionDate(it),
                student = Student(sessionStudentId(it), sessionStudentName(it)),
                amount = sessionAmount(it),
                taxYear = taxYear(sessionDate(it)), paid = false)
    }.toLens()
}
