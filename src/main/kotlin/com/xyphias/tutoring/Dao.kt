package com.xyphias.tutoring

import com.natpryce.Result
import com.natpryce.mapError
import com.natpryce.resultFrom
import java.sql.Date
import java.sql.DriverManager
import java.sql.ResultSet

class Dao(private val databaseUrl: String) {
    fun addSession(session: Session): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement =
                connection
                .prepareStatement("INSERT INTO Sessions(student_id, date, amount, tax_year) VALUES(?, ?, ?, ?)")
        statement.setInt(1, session.student.id!!)
        statement.setDate(2, Date.valueOf(session.date))
        statement.setInt(3, session.amount)
        statement.setInt(4, session.taxYear!!)

        statement.execute()

        connection.close()
    }

    fun addStudent(student: Student): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement = connection.prepareStatement("INSERT INTO Students(name) VALUES(?)")
        statement.setString(1, student.name)

        statement.execute()

        connection.close()
    }

    fun getSessions(): Result<List<Session>, Exception> = resultFrom {
        println("Before getting DB connection")
        val connection = DriverManager.getConnection(databaseUrl)
        println("After getting DB connection")
        val statement = connection.prepareStatement("""
            |SELECT se.id as session_id, se.date, se.amount, se.tax_year, se.paid, st.id as student_id, st.name
            |FROM Sessions se INNER JOIN Students st ON se.student_id = st.id ORDER BY date DESC""".trimMargin())
        val resultSet: ResultSet = statement.executeQuery()
        println("After executing query")
        val sessions = mutableListOf<Session>()
        while (resultSet.next()) {
            sessions.add(Session(resultSet.getInt("session_id"), resultSet.getDate("date").toLocalDate(),
                    Student(resultSet.getInt("student_id"), resultSet.getString("name")), resultSet.getInt("amount"),
                    resultSet.getBoolean("paid"), resultSet.getInt("tax_year")))
        }
        println("After processing result set")

        connection.close()

        sessions
    }

    fun getStudents(): Result<List<Student>, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement = connection.prepareStatement("SELECT * FROM Students")
        val resultSet: ResultSet = statement.executeQuery()

        val students = mutableListOf<Student>()
        while (resultSet.next()) {
            students.add(Student(resultSet.getInt("id"), resultSet.getString("name")))
        }

        connection.close()

        students
    }

    fun setTaxYear(taxYearUpdate: TaxYearUpdate): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        taxYearUpdate.sessionIds.forEach {
            val statement = connection.prepareStatement("UPDATE Sessions SET tax_year = ? WHERE id = ?")
            statement.setInt(1, taxYearUpdate.year)
            statement.setInt(2, it)
            statement.execute()
        }

        connection.close()
    }

    fun setPaid(paidUpdate: PaidUpdate): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement = connection.prepareStatement("UPDATE Sessions SET paid = ? WHERE id = ?")
        statement.setBoolean(1, paidUpdate.paid)
        statement.setInt(2, paidUpdate.sessionId)
        statement.execute()

        connection.close()
    }

    fun deleteSessions(sessionIds: Array<Int>): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement = connection.prepareStatement("DELETE FROM Sessions WHERE id = ?")

        sessionIds.forEach {
            statement.setInt(1, it)
            statement.addBatch()
        }

        statement.executeBatch()

        connection.close()
    }

    fun deleteStudent(studentId: Int): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)

        val statement = connection.prepareStatement("DELETE FROM Students WHERE id = ?")
        statement.setInt(1, studentId)
        statement.execute()

        connection.close()
    }
    fun updateSchema(scriptName: String): Result<Unit, Exception> = resultFrom {
        val connection = DriverManager.getConnection(databaseUrl)
        connection.autoCommit = true

        val updateScript = javaClass.getResource("/sql/$scriptName.sql").readText()

        val statement = connection.createStatement()
        statement.execute(updateScript)

        connection.close()
    }
}
