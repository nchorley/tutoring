package com.xyphias.tutoring

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import org.http4k.core.*
import org.http4k.core.cookie.Cookie
import org.http4k.core.cookie.cookie
import org.http4k.lens.*
import org.http4k.template.TemplateRenderer
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*

class LoginService(private val password: String, private val algorithm: Algorithm, private val templateRenderer: TemplateRenderer) {
    fun getLoginPage(request: Request): Response {
        return Response(Status.OK).body(templateRenderer(Login(null)))
    }

    fun login(request: Request): Response {
        val passwordField = FormField.string().nonEmptyString().required("password")
        val formBody = Body.webForm(Validator.Strict, passwordField).toLens()

        try {
            formBody.extract(request)
        } catch (e: LensFailure) {
            return errorResponse("Password must be supplied")
        }

        val form = formBody.extract(request)
        val requestPassword = passwordField.extract(form)

        if (requestPassword != password) {
            return errorResponse("Incorrect password")
        }

        val expiryDate = Date.from(Instant.now().plus(10, ChronoUnit.MINUTES))
        val token: String = JWT.create().withExpiresAt(expiryDate).sign(algorithm)
        val cookie = Cookie("tutoring_session", token, secure = true, httpOnly = true)

        return Response(Status.FOUND).header("Location", "/sessions").cookie(cookie)
    }

    private fun errorResponse(error: String) = Response(Status.BAD_REQUEST).body(templateRenderer(Login(error)))
}

class LoginFilter(algorithm: Algorithm): Filter {
    override fun invoke(next: HttpHandler): HttpHandler {
       return { request: Request ->
           val maybeSessionCookie = request.cookie("tutoring_session")

           if (maybeSessionCookie == null || !isValid(maybeSessionCookie.value)) {
               Response(Status.FOUND).header("Location", "/login")
           } else {
               next(request)
           }
       }
    }

    private fun isValid(token: String): Boolean {
        return try {
            verifier.verify(token)
            true
        } catch (e: JWTVerificationException) {
            false
        }
    }

    private val verifier: JWTVerifier = JWT.require(algorithm).build()
}
