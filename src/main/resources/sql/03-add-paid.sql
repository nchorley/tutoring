ALTER TABLE Sessions ADD COLUMN paid BOOLEAN;
UPDATE Sessions SET paid = true;
