CREATE TABLE Students(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100)
);

CREATE TABLE Sessions(
    id SERIAL PRIMARY KEY,
    student_id INTEGER REFERENCES Students,
    date DATE,
    amount INTEGER
);
