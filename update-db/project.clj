(defproject update-db "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"], [clj-http "3.9.1"]]
  :main ^:skip-aot update-db.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
