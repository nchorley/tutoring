(ns update-db.core
  (:gen-class)
  (:require [clojure.string :as string]
            [clj-http.client :as http-client]))

(declare valid? sql-scripts-in update-db)

(defn -main
  [& args]

  (if (not (valid? args))
    (do
      (println "Usage: update-db script-directory base-url")
      (System/exit 1)))

  (let [script-dir (first args)
        base-url (second args)]

    (run! (partial update-db base-url) (sql-scripts-in script-dir))))

(defn valid? [args]
  (= (count args) 2))

(defn remove-dir [path dir]
  (string/replace path dir ""))

(defn remove-sql-extension [file-name]
  (string/replace file-name ".sql" ""))

(defn files-in [dir]
  (rest (file-seq (java.io.File. dir))))

(defn sql-scripts-in [dir]
  (map
   #(-> % .getName (remove-dir dir) remove-sql-extension)
   (files-in dir)))

(defn update-db [base-url script]
  (let [response
        (http-client/post (str base-url "/db-update/" script))]
    (print (str script ": "))

    (if (= (:status response) 200)
      (print "OK")
      (print "Something went wrong"))

    (println)))
